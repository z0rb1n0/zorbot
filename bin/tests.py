#!/usr/bin/python3 -uB


import sys, os, argparse, time, logging, signal, pigpio, smbus

# we need to change our search path to seamlessly include our own modules
sys.path.insert(0, os.path.realpath(os.path.dirname(sys.argv[0]) + "/../lib"))

LOGGER = logging.getLogger(__name__)

import log_config

from bus import \
	gpio

from peripherals import \
	power, ADS, propulsion, frame



class MixedHelpFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawTextHelpFormatter):
	""" We need to preseve some formatting in the help string. Hence this """
	pass


def main():
	

	ap = argparse.ArgumentParser(
		description = "\n".join(map(str.strip, str("""

			Fire test patterns against Zorbot's control bus and shows the output

		""").split("\n"))),
		formatter_class = MixedHelpFormatter
	)

	#ap.add_argument("command_line", nargs = "*", help = "Example all-capturing positional argument")
	#actions_group = ap.add_mutually_exclusive_group()
	#actions_group.add_argument("--yes", dest = "yes", action = "store_true", required = False, default = argparse.SUPPRESS, help = "Do yes example")
	#actions_group.add_argument("--no", dest = "no", action = "store_true", required = False, default = argparse.SUPPRESS, help = "Do no example")
	#ap.add_argument("--required-str", dest = "required_str", metavar = "EXAMPLE_STR", type = str, required = True, help = "Example of required argument")
	#ap.add_argument("--optional-int", dest = "optional_int", metavar = "EXAMPLE_INT", type = int, required = False, default = 0, help = "Example of optional argument")


	ap.add_argument("--debug",
		dest = "debug",
		action = "store_true",
		required = False,
		default = argparse.SUPPRESS,
		help = "Debug mode. Causes errors and events to be more verbose and exceptions to be displayed"
	)

	cmd_args = ap.parse_args()

	log_config.set_logging_handlers(
		log_level = "DEBUG" if ("debug" in cmd_args) else "INFO",
		log_to_console = True
	)


	bus = smbus.SMBus(bus = 1)


	# bring up the power rail monitor and instantiate it
	ads = ADS.ADS1115(
		smbus_connection = bus,
		address = "GND",
		saturation_voltage = 4.096,
		sample_rate = 860
	)


	main_rail = power.VSource(
		vdc_meter = lambda : ads.get_voltage(0),
		max_current = 10,
		divider_ratio = 0.1,
		max_sample_age = 1
	)
	

	pibus = gpio.GPIO()


	frame_devices = {"cell_banks_or_psu": main_rail}

	for (pin, name) in {
		(18, "dc_betafpv_6x15mm_bl_orange"),
		(23, "dc_betafpv_6x15mm_br_green"),
		(24, "dc_betafpv_6x15mm_fl_brown"),
		(25, "dc_betafpv_6x15mm_fr_blue")
	}:
		pibus.pins[pin].io_mode = gpio.IOMode.PWM
		frame_devices[name] = propulsion.DCThruster(
			power_rail = frame_devices["cell_banks_or_psu"],
			control_pin = pibus.pins[pin],
			max_voltage = 4.0
		)



	copter = frame.Frame(devices = frame_devices)



	CYCLES_PER_SECOND = 50

	loop_allowance = 1.0 / float(CYCLES_PER_SECOND)
	


	main_thrusters = dict(filter(
		lambda i : isinstance(i[1], propulsion.DCThruster),
		copter.devices.items()
	))
	
	def signal_handler(sig, frame):
		LOGGER.info("Received signal `%d`. Shutting down", sig)
		copter.shutdown()
		pibus.close()
		sys.exit(0)
	
	
	[signal.signal(sig, signal_handler) for sig in {
		signal.SIGHUP, signal.SIGINT, signal.SIGTERM, signal.SIGQUIT
	}]
	
	while (True):
		loop_start = time.time()

		# reading the voltage from the ADS1115 is stupidly slow
		rail_voltage = frame_devices["cell_banks_or_psu"].get_sample()["vdc"]
		LOGGER.debug("Rail voltage is %.3fV", rail_voltage)

		for (pin_id, thruster) in main_thrusters.items():
			target_thrust = None
			thruster.thrust_fraction = target_thrust

		
		time_to_deadline = loop_allowance - (time.time() - loop_start)
		if (time_to_deadline > 0.0):
			LOGGER.debug("Sleeping for %.6f seconds", time_to_deadline)
			time.sleep(time_to_deadline)
		else:
			LOGGER.warning("Loop deadline missed by %.6f seconds" % -time_to_deadline)
		

	
	

	pibus.close()

	return 0





	bus.close()







if (__name__ == "__main__"):
	sys.exit(main())





