#!/usr/bin/python3 -uB


import sys, os, argparse, time, logging

# we need to change our search path to seamlessly include our own modules
sys.path.insert(0, os.path.realpath(os.path.dirname(sys.argv[0]) + "/../lib"))

LOGGER = logging.getLogger(__name__)

import log_config

from pymata_aio.pymata3 import PyMata3
from pymata_aio.constants import Constants




def main():


	"""
	Set digital pin 6 as a PWM output and set its output value to 128
	@return:
	"""
	# instantiate the pymata_core API
	board = PyMata3()
	

	import pprint
	help(board.core)
	exit()
	pprint.pprint(board.core.__dict__)
	#print(board.get_capability_report(raw = False))


# 	# set the pin mode
# 	board.set_pin_mode(6, Constants.PWM)
# 	board.analog_write(6, 128)

# 	# wait for 3 seconds to see the LED lit
# 	board.sleep(3)

	# reset the board and exit
	board.shutdown()


	return 0


if (__name__ == "__main__"):
	try:
		sys.exit(main())
	except KeyboardInterrupt as e_int:
		LOGGER.info("Received SIGINT")
		pass





