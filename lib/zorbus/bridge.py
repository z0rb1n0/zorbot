#!/usr/bin/python -uB

"""
	Module to interact with ZorBridge "peripherals"
"""

import \
	logging, urllib.parse, io, importlib


LOGGER = logging.getLogger(__name__)



class ZorBridge(io.IOBase):


	"""
		Abstraction of a Pin Bridge the ZorBus can integrate
		(eg: this could represent an arduino whose pins/i2c bus we want
		to proxy and manage the same way as the rest)
	"""
	def __init__(self,
		gateway_url = "rpi://gpio",
		transport_url = None
	):
		"""

			Some examples of Transport URL
			uart://ttyUSB0?bps=57600&bytesize=8&stopbits=1

			# i2c is not supported yet, but here's how the URL would look
			i2c://bus/address


			Some examples of Bridge Processor URL
			
			rpi://			# has no options so far as the list of interactables is discovered
			arduino://		# has no options so far as the list of interactables is discovered



			########################################
			#### On gateway drivers
			########################################
			
			Gateway drivers are expected to implement a Processor class


			Expected methods and signatures in Processor class:

			__init__():		expects the following parameters:
									location:	(str)"netloc" part of the URL
									path:		(str)"path" part of the URL
									query:		(dict)Key value-pair query string arguments
									connection:	(Connection)A connection object from a transport driver
			
			                  The driver is free to interpred URL components however it likes

			set_connection():	allows one to change transport in-flight by passing a different connection here
			rescan():			re-generates the internal list of pins and other interactables.
			push_config():		pushes state-change commands to re-align real pins with the objects' statuses
			read_pins():		re-reads pin values and updates the pin opjects in "pins". Returns a dictionary of values
								In order to limit traffic, it must accept an optional list of pins to restrict
			push_pins():		Pushes the values in the "pins" dictionary object down the pins.
								Supports the same argument as read_pins

			shutdown():			Clear all resources and stop working. Must be called by the destructor too
			reboot():			When applicable & safe, the gateway is re-initialized (eg: arduino reboot).
			pins:				dictionary of pins indexed by number. Not a list as some pins are not available
								Each member is an object of type gwio.Pin

			i2c:				Analogous to analog and digital pins, this is the list of I2C addresses that were
								detective on the bus. Object type: I2CTarget






			########################################
			#### On transport drivers
			########################################

			Transport drivers are expected to implement a Connection class

			# Expected members and signatures in a Connection class:

			# __init__():		exact semantics as for gateway drivers, just without the connection
			# open():			establishes the connection. Should always be called by the constructor
			# send():			same semantics in the socket library (whenever flags are applicable)
			# recv():			same semantics in the socket library (whenever flags are applicable)
			# recv_into():		same semantics in the socket library (whenever flags are applicable)
			# set_timeout():	Timeout for I/O operations.
			# close():			shuts down. Should always be called by the destructor


			# All methods return what you'd expect from the any "io"-like method



			# Both gateway and transport drivers must advertise the
			# query string arguments they accept in a  ZORBUS_ARGS member (list)
			# Multiple instances of a given argument in the URL are forbidden



			Args:
				gateway_url:			(str)Gateway URL string (see above)
				transport_url:			(str)Transport URL string (see above)

		"""

		# processing & sanitation
		g_url = urllib.parse.urlparse(gateway_url)
		t_url = (None if (transport_url is None) else urllib.parse.urlparse(transport_url))


		g_args = t_args = {}
		for (url_scope, parsed_url) in [
			("gateway", g_url),
			("transport", t_url)
		]:
			if (parsed_url is None):
				continue
			qs_args = urllib.parse.parse_qs(parsed_url.query)
			for (arg_name, arg_values) in qs_args.copy().items():
				if (len(arg_values) == 1):
					qs_args[arg_name] = arg_values[0]
				else:
					raise ValueError("Query string argiument `%s` is repeated in the %s address string" % (qs_arg, url_scope))

			# route the values in the right dictionary
			if (url_scope == "gateway"):
				g_args = qs_args
			else:
				t_args = qs_args
	
	
		self.connection = None
		self.processor = None


		# time to load things

		# Transport must be initialised first as the gateway may already use it
		# during initialisation
		if (t_url is not None):

			LOGGER.info("Establishing transport connection at address `%s://%s/%s?%s`", 
				t_url.scheme, t_url.netloc, t_url.path, t_url.query
			)

			try:
				t_driver = importlib.import_module("zorbus.transports." + t_url.scheme)
			except ImportError as e_imp:
				raise ImportError("Unable to load transport driver `%s`" % t_url.scheme)


			if (any(set(qs_args).difference(t_driver.ZORBUS_ARGS))):
				raise ValueError("Transport driver `%s` does not support the following options: %s" %
					(t_driver.__name__, ", ".join(set(qs_args).difference(t_driver.ZORBRIDGE_ARGS)))
				)


			self.connection = t_driver.Connection(
				location = t_url.netloc,
				path = t_url.path,
				query = t_args
			)

		LOGGER.info("Loading gateway at address `%s://%s/%s?%s`", 
			g_url.scheme, g_url.netloc, g_url.path, g_url.query
		)

		try:
			g_driver = importlib.import_module("zorbus.gateways." + g_url.scheme)
		except ImportError as e_imp:
			raise ImportError("Unable to load gateway driver `%s`" % g_url.scheme)


		self.processor = g_driver.Processor(
			location = g_url.netloc,
			path = g_url.path,
			query = g_args,
			connection = self.connection
		)

		# we proxy some of the processor's methods
		for proxy_attr in {"read_pins", "push_pins"}:
			setattr(self, proxy_attr, getattr(self.processor, proxy_attr))
