#!/usr/bin/python -uB

"""
	Common bus abstracting away GPIO/arduino/I2C proxies
"""

import \
	logging, 


LOGGER = logging.getLogger(__name__)

class ZorBus():
	"""
		Allows one to handle any pin/reference voltage/I2C address lane
		the same way regardless of whether or not they're local or proxied by, say,
		an Arduino proxying them via serial/I2C.
		
		SPI proxying is not yet implemented
	"""
	def __init__(self, url = "serial://"):
		
		# This is a dictionary indexed by arbitrary, unique bus names,
		# Members will contain the bridge object itself
		self.buses = {}



	def attach(transport, driver, identifier):
		"""
			Connects a backend to 
		"""
