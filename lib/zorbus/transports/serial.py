#!/usr/bin/python -uB

"""
	Serial port module for Zorbot.
"""

import os, logging, serial


LOGGER = logging.getLogger(__name__)


# arguments that will be fed to the Serial constructor if all goes well.
# We rewrite these as needed in the object member's copy
DEFAULT_SERIAL_PORT = "ttyUSB0"
DEFAULT_SERIAL_ARGS = {
	"baudrate": 57600,
	"bytesize": 8,
	"parity": "N",
	"stopbits": 1
}

ZORBUS_ARGS = set(DEFAULT_SERIAL_ARGS)

class Connection():
	"""
		Implements the serial connection backend for the driver.
		
		Expects the parsed urllib object (ignores the scheme)
	"""

	def __init__(self, location, path, query):

		self.backend = None
		# we keep a single dictionary
		self.serial_args = dict(DEFAULT_SERIAL_ARGS, **{"port": DEFAULT_SERIAL_PORT})

		self.open(location = location, path = path, query = query)


	def open(self, location = None, path = None, query = None):


		# In the future we may support remote ports, which would look like
		# serial://host.name/ttyUSB0, serial://host.name/dev/ttyUSB0
		# or serial://host.name/COM0
		# 
		# For now we just need to determine whether the port we're gonna use comes from
		# netloc or path

		if ((location is not None) or (path is not None)):
			if (len(path or "")):
				self.serial_args["port"] = (path or "").strip()
			else:
				c_loc = (location or "").strip()

				if (len(c_loc)): # we ignore empty locations
					self.serial_args["port"] = c_loc
				
			if ((not self.serial_args["port"].startswith("/")) and (os.name == "posix")):
				self.serial_args["port"] = "/dev/" + self.serial_args["port"]


		# override of defaults from arguments and sanitation
		if (query is not None):
			for s_arg in query:
				if (s_arg in self.serial_args):
					# what we use to cast the value depends on the setting
					cast_f = int if (s_arg in ["baudrate", "bytesize", "stopbits"]) else str.strip
					self.serial_args[s_arg] = cast_f(qs_args[s_arg])


		try:
			self.backend = serial.Serial(**self.serial_args)
		except serial.serialutil.SerialException as e_serial:
			LOGGER.error("Serial port error on `%s`: %s(%s)",
				self.serial_args["port"], e_serial.__class__.__name__, e_serial
			)
			raise

		return self.backend


	def close(self):
		"""	Like the tin says """
		closure = self.backend.close()
		self.backend = None
		return closure
		


	def __del__(self):
		self.close()



	def send(self, data, flags = 0):
		if (self.backend is None):
			raise IOError("Serial connection is not open")
		return self.backend.write(data)


	def recv(self, buffersize, flags = 0):
		if (self.backend is None):
			raise IOError("Serial connection is not open")
		return self.backend.read(size = buffersize)

	def recv_into(self, buffer, nbytes, flags = 0):
		#  the serial module does not allow direct write-to-buffer. We fake it
		in_data = self.recv(nbytes)
		l = len(in_data)
		mv = memoryview(buffer)
		mv[0:l] = in_data
		return l
