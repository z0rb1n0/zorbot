#!/usr/bin/python -uB

"""
	Gateway module for ZorBus control of local RPI pins & busses
"""

import \
	logging, \
	RPi.GPIO as GPIO


from zorbus import \
	gwio


LOGGER = logging.getLogger(__name__)



ZORBUS_ARGS = set()

# highest pin address we look into
PIN_SCAN_MAX = 0xff
PWM_FREQUENCY = 250





class Processor():



	def __init__(self, location, path, query, connection):

		# It'd be nice if we could use the broadcom numbering scheme, but
		# that seems to change significantly across board revisions
		if (not (GPIO.getmode == GPIO.BOARD)):
			LOGGER.debug("Setting GPIO mode to %d", GPIO.BOARD)
			GPIO.setmode(GPIO.BOARD)

		self.pins = {}

		# This is a dirty trick as the PWM module is object-oriented
		# and we've got nowher to store these in. Indexed by pin_id
		self._pin_pwm_objects = {}

		self.rescan()


	def set_connection(self, connection):
		pass # no-op


	def rescan(self):

		# scan pin numbers to find out what pins do exist
		# and how they're configured
		for pin_id in range(PIN_SCAN_MAX):

			try:

				gpio_f = GPIO.gpio_function(pin_id)

				# we detect the port mode at first
				io_mode = {
					GPIO.IN: gwio.IOMode.InputDigital,
					GPIO.OUT: gwio.IOMode.OutputDigital
				}.get(gpio_f)

				if (io_mode is None):
					LOGGER.info("Pin %d is not in a supported state (%d) and will not be mapped", pin_id, gpio_f)
					continue

				self.pins[pin_id] = gwio.Pin(
					io_mode = io_mode
				)


			except ValueError as e_nopin:

				# Normal, given that we're pretty much brute-forcing a range,
				# but we cleanup if a pin disappeared

				if (pin_id in self.pins):
					LOGGER.warning("Pin %d disappeared" % pin_id)
					del(self.pins[pin_id])



	def push_config(self):

		for (pin_id, pin_obj) in self.pins.items():

			# some craftiness with the log string
			log_str = "Configuring pin #%d for %s"
			log_args = [pin_id, pin_obj.io_mode.name]
			if (pin_obj.io_mode in [gwio.IOMode.InputDigital, gwio.IOMode.InputAnalog]):
				log_str += ", wired in %s mode"
				log_args += [pin_obj.wiring_mode.name]
			LOGGER.debug(log_str, *log_args)

			#  translate the enumerator mode into RPi numbers
			rpi_direction = {
				gwio.IOMode.InputDigital:	GPIO.IN,
				gwio.IOMode.OutputDigital:	GPIO.OUT,
				gwio.IOMode.OutputPWM:	GPIO.OUT
			}.get(pin_obj.io_mode)

			if (rpi_direction is None):
				raise NotImplementedError("Unsupported mode for pin #%d (%s)" % (
					pin_id, pin_obj.io_mode.name
				))

			rpi_pud = {
				gwio.WiringMode.Floating: GPIO.PUD_OFF,
				gwio.WiringMode.PullDown: GPIO.PUD_DOWN,
				gwio.WiringMode.PullUp: GPIO.PUD_UP
			}.get(pin_obj.wiring_mode)

			if (rpi_pud is None):
				raise NotImplementedError("Unsupported wiring mode fir pin #%d (%s)" % (
					pin_id, pin_obj.wiring_mode.name
				))

			GPIO.setup(pin_id, rpi_direction, rpi_pud)

			# We need to re-align PWM state to what's configured here.
			# Tricky change due to the OOB model in RPi.GPIO,
			# which is the only class
			pwm_needed = (pin_obj.io_mode == gwio.IOMode.OutputPWM)
			pwm_configured = (pin_id in self._pin_pwm_objects)
			if (pwm_configured != pwm_needed):
				if (not pwm_configured):
					# create PWM object
					self._pin_pwm_objects[pin_id] = GPIO.PWM(pin_id, PWM_FREQUENCY)
					self._pin_pwm_objects[pin_id].start(0)
				else:
					# delete PWM object if needed
					if (pwm_configured):
						self._pin_pwm_objects[pin_id].stop()
						del(self._pin_pwm_objects[pin_id])



	def read_pins(self, pins = None):


		for pin_id in set(pins if (pins is not None) else self.pins):

			pin_o = self.pins.get(pin_id)

			if (pin_o is None):
				raise KeyError("No such pin numer: %d" % pin_id)

			if (pin_o.io_mode not in [gwio.IOMode.InputDigital, gwio.IOMode.InputAnalog]):
				raise gwio.PinStateError("Pin #%d %s mode, which is invalid for input" % (pin_id, pin_o.io_mode.name))

			# the cast will take care of it
			pin_o.value = GPIO.input(pin_id)


	def push_pins(self, pins = None):

		for pin_id in set(pins if (pins is not None) else self.pins):
			pin_o = self.pins.get(pin_id)
			if (pin_o.io_mode == gwio.IOMode.OutputPWM):
				# PWM is expressed in percentages in RPi.GPIO.PWM
				self._pin_pwm_objects[pin_id].ChangeDutyCycle(round(100.0 * pin_o.value, 2))
			else:
				# just set the pin's value
				GPIO.output(pin_id, pin_o.value)



	def shutdown(self):
		LOGGER.debug("Cleaning up...")
		# if any pin is in PWM, we need to stop that
		for pin_id in self._pin_pwm_objects:
			LOGGER.debug("Disabling PWM on Pin #%d", pin_id)
			self._pin_pwm_objects[pin_id].stop()


		self.pins = self._pin_pwm_objects = {}

		GPIO.cleanup()

	def __del__(self):
		self.shutdown()
