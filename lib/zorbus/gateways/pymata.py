#!/usr/bin/python -uB

"""
	Gateway module for arduino-zorbot
"""

import \
	time, logging, \
	pymata_aio.pymata3 as pymata, \
	pymata_aio.constants as pymcon




from zorbus import \
	gwio

# from pymata_aio.pymata3 import \
# 	PyMata3


# from pymata_aio.constants import \
# 	Constants as pymata_constants


LOGGER = logging.getLogger(__name__)



ZORBUS_ARGS = set()

DEFAULT_BAUDRATE = 57600



class Processor():


	def __init__(self, location, path, query, connection):
		"""
			Location maps to the port
			
			
			
			Although the gateway driver uses a serial port, it does so independently
			and does not need a serial connection
		"""

		self.line = None
		self.pins = {}
		self.set_connection(connection)

		# Real pin numbers are not reflected in the array indices.
		# We keep our own dictionary
		self._pin_maps = {}


		serial_port = None
		if ((location or "").strip() != ""):
			serial_port = "/dev/" + location.strip()


		self.board = pymata.PyMata3(
			com_port = serial_port
		)

		self.rescan()


	def set_connection(self, connection):
		# pyfirmata handles its own serial
		pass


	def rescan(self):


		detected_pins = set()
		for b_pin in self.board.core.digital_pins + self.board.core.analog_pins:
			print(b_pin.__dict__)
			continue;
			detected_pins.add(b_pin.pin_number)

			io_mode = {
				firmata.INPUT:
					gwio.IOMode.InputAnalog if (b_pin.type == firmata.ANALOG) else gwio.IOMode.InputDigital
				,
				firmata.OUTPUT: gwio.IOMode.OutputDigital,
				firmata.PWM:  gwio.IOMode.OutputPWM
			}.get(b_pin._mode) # wonder why it's marked private

			if (io_mode is None):
				LOGGER.info("Pin %d is not in a supported state (%d) and will not be mapped", b_pin.pin_number, b_pin._mode)
				continue

			self.pins[b_pin.pin_number] = gwio.Pin(
				io_mode = io_mode
			)
			self._pin_maps[b_pin.pin_number] = b_pin

		exit(0)
		# inverse scan to remove pins that mysteriously vanished
		for o_pin in self.pins:
			if (o_pin not in detected_pins):
				LOGGER.warning("Pin %d disappeared" % pin_id)
				del(self._pin_maps[pin_id])
				del(self.pins[pin_id])


		return 0


	def push_config(self):

		for (pin_id, pin_obj) in self.pins.items():

			# some craftiness with the log string
			log_str = "Configuring pin #%d for %s"
			log_args = [pin_id, pin_obj.io_mode.name]
			if (pin_obj.io_mode in [gwio.IOMode.InputDigital, gwio.IOMode.InputAnalog]):
				log_str += ", wired in %s mode"
				log_args += [pin_obj.wiring_mode.name]
			LOGGER.debug(log_str, *log_args)

			if (
					((pin_obj.io_mode == gwio.IOMode.OutputPWM) and (not self._pin_maps[pin_id].PWM_CAPABLE))
				or
					((pin_obj.io_mode == gwio.IOMode.InputAnalog) and (self._pin_maps[pin_id].type != firmata.ANALOG))
			):
				raise gwio.PinStateError("Pin %d does not support %s mode!" % (pin_id, pin_obj.io_mode.name))



			#  translate the enumerator mode into firmata numbers
			rpi_direction = {
				gwio.IOMode.InputDigital:	firmata.INPUT,
				gwio.IOMode.OutputDigital:	firmata.OUTPUT,
				gwio.IOMode.OutputPWM:	GPIO.OUT
			}.get(pin_obj.io_mode)

			if (rpi_direction is None):
				raise NotImplementedError("Unsupported mode for pin #%d (%s)" % (
					pin_id, pin_obj.io_mode.name
				))

			rpi_pud = {
				gwio.WiringMode.Floating: GPIO.PUD_OFF,
				gwio.WiringMode.PullDown: GPIO.PUD_DOWN,
				gwio.WiringMode.PullUp: GPIO.PUD_UP
			}.get(pin_obj.wiring_mode)

			if (rpi_pud is None):
				raise NotImplementedError("Unsupported wiring mode fir pin #%d (%s)" % (
					pin_id, pin_obj.wiring_mode.name
				))

			GPIO.setup(pin_id, rpi_direction, rpi_pud)

			# We need to re-align PWM state to what's configured here.
			# Tricky change due to the OOB model in RPi.GPIO,
			# which is the only class
			pwm_needed = (pin_obj.io_mode == gwio.IOMode.OutputPWM)
			pwm_configured = (pin_id in self._pin_pwm_objects)
			if (pwm_configured != pwm_needed):
				if (not pwm_configured):
					# create PWM object
					self._pin_pwm_objects[pin_id] = GPIO.PWM(pin_id, PWM_FREQUENCY)
					self._pin_pwm_objects[pin_id].start(0)
				else:
					# delete PWM object if needed
					if (pwm_configured):
						self._pin_pwm_objects[pin_id].stop()
						del(self._pin_pwm_objects[pin_id])



	def read_pins(self, pins = None):

		return 0
		for pin_id in set(pins if (pins is not None) else self.pins):

			pin_o = self.pins.get(pin_id)

			if (pin_o is None):
				raise KeyError("No such pin numer: %d" % pin_id)

			if (pin_o.io_mode not in [gwio.IOMode.InputDigital, gwio.IOMode.InputAnalog]):
				raise gwio.PinStateError("Pin #%d %s mode, which is invalid for input" % (pin_id, pin_o.io_mode.name))

			# the cast will take care of it
			pin_o.value = GPIO.input(pin_id)


	def push_pins(self, pins = None):

		return 0
		for pin_id in set(pins if (pins is not None) else self.pins):
			pin_o = self.pins.get(pin_id)
			if (pin_o.io_mode == gwio.IOMode.OutputPWM):
				# PWM is expressed in percentages in RPi.GPIO.PWM
				self._pin_pwm_objects[pin_id].ChangeDutyCycle(round(100.0 * pin_o.value, 2))
			else:
				# just set the pin's value
				GPIO.output(pin_id, pin_o.value)



	def shutdown(self):
		self.board.shutdown()
		time.sleep(5)
		return 0


	def __del__(self):
		self.shutdown()
