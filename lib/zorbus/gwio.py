#!/usr/bin/python -uB

"""
	Common bus abstracting away GPIO/arduino/I2C proxies
"""

import \
	enum, \
	logging


LOGGER = logging.getLogger(__name__)



@enum.unique
class IOMode(enum.IntEnum):

	InputDigital = 1
	InputAnalog = 2

	OutputDigital = 3
	OutputPWM = 4



@enum.unique
class WiringMode(enum.IntEnum):

	Floating = 0
	PullDown = 1
	PullUp = 2



class PinStateError(Exception):
	""" Raised when the pin is in a wrong state """




class NormalizedFraction(float):
	"""	Simple validation class. """
	def __init__(self, val):
		if (not (0.0 <= val <= 1.0)):
			raise ValueError("Value out of boundaries of normalized floats (0.0 to 1.0): %.3f" % val) 




class Pin():
	"""
		Implementation class for generic capabilities of pins
	"""
	def __init__(self,
		io_mode = IOMode.InputDigital,
		wiring_mode = WiringMode.Floating
	):
		"""
			Just a generic Pin representation

			Args:
				io_mode:		(IOMode)I/O Mode to initialise the pin in.
								If None is passed, then it is discovered instead
				wiring_mode:	(WiringMode)What to connect the pin to.
				analog_range:	(2-tuple)Bounds of integers analog input is
								mapped to. None is used to indicate
								digital-only pins
				pwm_range:		(2-tuple)Bounds of integers PWM-capable pins
								can accept to indicate fraction of duty cycle.
								None is used to indicate non PWM-capable pins

		"""

		# Initialization, basic cleanup & early hard fails
		self._value_cast = None # this will be a callable that validates/casts pin values
		self.io_mode = io_mode
		self.wiring_mode = wiring_mode
		self._value = None


	def _get_supported_modes(self):
		""" Convenience list of capabilities """
		return {
			IOMode.InputDigital,
			IOMode.OutputDigital
		}.union(
			{
				IOMode.InputAnalog,
			} if (self.analog_range is not None) else set(),
			{IOMode.OutputPWM} if (self.pwm_range is not None) else set()
		)
	supported_modes = property(fget = _get_supported_modes)



	def _get_io_mode(self):
		return self._io_mode


	def _set_io_mode(self, mode):

		# Different modes change the way inputs and outputs are translated/accepted
		if (mode in [IOMode.InputDigital, IOMode.OutputDigital]):
			self._value_cast = bool
		elif (mode in [IOMode.InputAnalog, IOMode.OutputPWM]):
			# normalised 0.0 - 1.0
			self._value_cast = NormalizedFraction
		else:
			raise ValueError("Unsupported Pin mode: `%s`" % mode)

		self._io_mode = IOMode(mode)

	io_mode = property(fget = _get_io_mode, fset = _set_io_mode)




	def _get_wiring_mode(self):
		return self._wiring_mode

	def _set_wiring_mode(self, mode):
		self._wiring_mode = WiringMode(mode)

	wiring_mode = property(fget = _get_wiring_mode, fset = _set_wiring_mode)




	def _get_value(self):
		return self._value

	def _set_value(self, value):
		self._value = self._value_cast(value)

	value = property(fget = _get_value, fset = _set_value)


	def __text__(self):

		# We show all supported modes, but just highlight the ones we know
		desc_str = "PIN{IO_MODES: %s" % (", ".join([(
				("[" if (m == self.io_mode) else "")
			+
				m.name
			+
				("]" if (m == self.io_mode) else "")
		) for m in sorted(IOMode)]))

		# only input pins display a wiring mode and a value
		if (self.io_mode in [IOMode.InputDigital, IOMode.InputAnalog]):
			desc_str += ", WIRING_MODES: %s" % (", ".join([(
					("[" if (m == self.wiring_mode) else "")
				+
					m.name
				+
					("]" if (m == self.wiring_mode) else "")
			) for m in sorted(WiringMode)]))

			desc_str += ", VALUE: %s" % self.value

		desc_str += "}"

		return desc_str

	def __repr__(self):
		return "< " + self.__text__() + " >"
