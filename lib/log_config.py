#!/usr/bin/python3 -uB
import sys, os, stat, logging, logging.config, logging.handlers, traceback

"""
	This module simply configures the logger the way we like.
	It is here mostly to eliminate boilerplate
"""


# until we implement relaying logs to multiple destinations,
# we simply disable syslog if the socket is not found. Here's
# the expected path
SYSLOG_SOCKET = "/dev/log"

SYSLOG_FACILITY = logging.handlers.SysLogHandler.LOG_LOCAL3



# This is how the console messages are shown by default.

#  For console, a timestamp at the front can be added if needed
BASE_FORMAT_CONSOLE = "%(levelname)-10s %(name)-32s: %(message)s"

# Syslog always timestamps messages regardless, and we don't need internal
# high precision timestamps, so no timestamp here
BASE_FORMAT_SYSLOG = "%s[%%(process)d]: %%(name)-32s %%(message)s" % os.path.basename(sys.argv[0])


LOGGER = logging.getLogger(__name__)


def uncaught_exception_handler(the_exception_class, the_exception, the_traceback):
	"""
		Generic exception handler that logs the exception instead of using standard streams.
	"""
	#We use the frame's own logger if available, otherwise the root logger.
	#we could scan the scope for loggers if naming conventions become less strict.
	target_logger = (the_traceback.tb_frame.f_globals["LOGGER"] if ("LOGGER" in the_traceback.tb_frame.f_globals) else logging)
	target_logger.error("Uncaught exception:")
	# the following join/split message is because we want to just have one log message per line
	for f_exc_line in filter(lambda l : len(l.rstrip()), "\n".join(traceback.format_exception(the_exception_class, the_exception, the_traceback)).split("\n")):
		target_logger.info("    " + f_exc_line)



def bootstrap_logging_handlers(apply_config = True):
	"""
		Configures/resets, a bare bones logger that can work with no configuration
		(it simply logs everything against stderr)
		
		
		Args:
			apply_config:		(bool)Whether or not to effectively apply the logging configuration or just dump it
		
		Returns:
			The configuration dictionary that was set up.
			Fails fatally in case of errors
	"""
	
	# This should be a module variable, however I've had some trouble safely
	# passing that around as deepcopy does not seem to like file streams
	out_config = {
		"version": 1,
		"disable_existing_loggers": False,
		"formatters": {
			"console": {"format": BASE_FORMAT_CONSOLE}
		},
		"handlers": {
			"console": {
				"formatter": "console",
				"class": "logging.StreamHandler",
				"stream": sys.stderr
			}
		},
		"loggers": {
			"": {
				"level": "INFO",
				# the list of handlers is reconfigured later
				"handlers": [ "console" ]
			}
		}
	}
	if (apply_config):
		logging.config.dictConfig(out_config)
		# all exceptions are logged as opposed to dumped from now on
		sys.excepthook = uncaught_exception_handler
		#LOGGER.debug("Bootstrap logging configuration successfully loaded")
	return out_config



def set_logging_handlers(log_level = "INFO", log_to_console = True, apply_config = True):
	"""
		Globally configures the logger.

		Args:
			log_level:					(str)Log level to set as a minimum threshold for the logger (INFO, DEBUG...see logger docs)
			log_to_console:				(bool)Whether or not log messages should be sent to the console handler
			apply_config:				(bool)Whether or not to effectively apply the logging configuration or just dump it

		Returns:
			The configuration dictionary that was set up.
			Fails fatally in case of errors
	"""

	# we start with a bare-bones configuration dictionary
	
	logging_config = bootstrap_logging_handlers(apply_config = False)

	# We need to omit syslog from the list of available handlers or the logger will fail
	# to initialize due to a missing /dev/log


	# do we have a usable syslog socket?
	has_syslog_socket = False
	try:
		has_syslog_socket = stat.S_ISSOCK(os.stat(path = SYSLOG_SOCKET, follow_symlinks = True).st_mode)
	except FileNotFoundError as e_nosyslog:
		pass


	if (has_syslog_socket):
		if (not log_to_console):
			# Log to console was not requested and syslog is available.
			# We inhibit console logging as syslog is enough
			logging_config["loggers"][""]["handlers"].remove("console")
			del(logging_config["handlers"]["console"])
			del(logging_config["formatters"]["console"])


		# We can configure the syslog handler
		logging_config["formatters"]["syslog"] = {"format": BASE_FORMAT_SYSLOG}
		logging_config["handlers"]["syslog"] = {
				"formatter": "syslog",
				"address": SYSLOG_SOCKET,
				"class": "logging.handlers.SysLogHandler",
				"facility": SYSLOG_FACILITY
		}
		logging_config["loggers"][""]["handlers"].append("syslog")
		logging_config["loggers"][""]["level"] = log_level

	else:
		sys.stderr.write("%s is not available as a socket. This can mean that your logger is not running, or that you're runnig in a namespace/container\n" % SYSLOG_SOCKET)
		sys.stderr.write("Logging to syslog will not be available and all messages will be emitted on the system's stderr regardless of configuration\n")



	# The console output format requires a timestamp only if
	# console output was specifically requested. If we're just falling back
	# to it, we don't care
	if (log_to_console):
		logging_config["formatters"]["console"]["format"] = "%(asctime)s " + logging_config["formatters"]["console"]["format"]


	if (apply_config):
		logging.config.dictConfig(logging_config)

		# see above
		sys.excepthook = uncaught_exception_handler
		LOGGER.debug("Final logging configuration successfully loaded")

	
	return logging_config


