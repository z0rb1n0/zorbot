#!/usr/bin/python3 -uB
"""
	ADS1015/ADS1115 module.
	
	Built from https://cdn-shop.adafruit.com/datasheets/ads1115.pdf
"""

import sys, os, time


# Possible addresses of the device, based on whath the ADDR pin is wired to.
SUPPORTED_ADDRESSES = {
	"GND": 0x48,
	"VDD": 0x49,
	"SDA": 0x4a,
	"SCL": 0x4B
}
DEFAULT_I2C_ADDRESS = "GND"


CONVERSION_POLL_INTERVAL = 0.0001 # in seconds


# size, in bytes, of the config register
CONFIG_SIZE = 2
# Sample size is always 2 bytes even for 12 bit ADCs, just the lower
# 4 bits that are retrieved via i2c are never set
SAMPLE_SIZE = 2

################################
# Possible operational statuses. Single most significant bit
################################


CONFIG_OFFSET_OPERATIONAL_STATUS = 15
CONFIG_MASK_OPERATIONAL_STATUS = 0x1 << CONFIG_OFFSET_OPERATIONAL_STATUS

# How they're set
OPERATIONAL_STATUS_BITS = {
	False:			0x0 << CONFIG_OFFSET_OPERATIONAL_STATUS,
	True:			0x1 << CONFIG_OFFSET_OPERATIONAL_STATUS
}
# the following is useful for reverse lookups
OPERATIONAL_STATUSES = {b: s for (s, b) in OPERATIONAL_STATUS_BITS.items()}

# The same bits return the "converting" state when read. Here's an
# alias to represent what they mean when read (aka: is the device converting?)
# The difference is that the evaluation is reversed (0 = busy converting)
CONFIG_MASK_CONVERTING_STATUS = CONFIG_MASK_OPERATIONAL_STATUS
CONVERTING_STATUS_BITS = dict([(not b, f) for (b, f) in OPERATIONAL_STATUS_BITS.items()])
CONVERTING_STATUSES = {b: s for (s, b) in CONVERTING_STATUS_BITS.items()}


################################################################
# Channels configuration. 3 bits at offset 12
################################################################

TOTAL_CHANNELS = 4

# Multiplexer configuration:

# 3 bits at offset 12 in the config register determine what's read and decoded.
# When MUX_MODE_CHANNEL_NUMBER is not set, the remaining bits in the multiplexer
# configuration are used as one of the possible differential pairs defined in
# MUX_MODE_DIFFERENTIAL_BITS.
# When MUX_MODE_CHANNEL_NUMBER is set, the remaining bits are just indicating
# the channel number

CONFIG_OFFSET_MUX_MODE = 12 # offset of the multiplexer mode in the config registry
CONFIG_MASK_MUX_MODE = 0x7 << CONFIG_OFFSET_MUX_MODE

MUX_MODE_CHANNEL_NUMBER = 0x4 << CONFIG_OFFSET_MUX_MODE
MUX_MODE_DIFFERENTIAL_BITS = {
	(0, 1): 0x0 << CONFIG_OFFSET_MUX_MODE,
	(0, 3): 0x1 << CONFIG_OFFSET_MUX_MODE,
	(1, 3): 0x2 << CONFIG_OFFSET_MUX_MODE,
	(2, 3): 0x3 << CONFIG_OFFSET_MUX_MODE
}
# the following is useful for reverse lookups
MUX_MODE_DIFFERENTIAL_PAIRS = {f: p for (p, f) in MUX_MODE_DIFFERENTIAL_BITS.items()}


################################################################
# Gain. 3 bits at offset 9
################################################################

CONFIG_OFFSET_GAIN = 9
CONFIG_MASK_GAIN = 0x7 << CONFIG_OFFSET_GAIN

# Values of gain setting registers,
# Indexed by saturation voltage (+/-)
GAIN_LEVELS = {
	6.144: 0x00 << CONFIG_OFFSET_GAIN,
	4.096: 0x01 << CONFIG_OFFSET_GAIN,
	2.048: 0x02 << CONFIG_OFFSET_GAIN,
	1.024: 0x03 << CONFIG_OFFSET_GAIN,
	0.512: 0x04 << CONFIG_OFFSET_GAIN,
	0.256: 0x05 << CONFIG_OFFSET_GAIN
}
# the following is useful for reverse lookups
SATURATION_VOLTAGES = {g: v for (v, g) in GAIN_LEVELS.items()}
DEFAULT_SATURATION_VOLTAGE = 4.096


################################################################
# Continuous conversion. A single bit at offset 8
################################################################
CONFIG_OFFSET_CONTINUOUS_MODE = 8
CONFIG_MASK_CONTINUOUS_MODE = 0x1 << CONFIG_OFFSET_CONTINUOUS_MODE

CONTINUOUS_MODE_BITS = {
	True:		0x0 << CONFIG_OFFSET_CONTINUOUS_MODE,
	False:		0x1 << CONFIG_OFFSET_CONTINUOUS_MODE
}
# the following is useful for reverse lookups
CONTINUOUS_MODE_STATUSES = {b: s for (s, b) in CONTINUOUS_MODE_BITS.items()}


################################################################
# Data rates. 3 bits at offset 5
# Implemented independently for ADS1015 and ADS1115.
# We index the dictionaries based on class name
################################################################
CONFIG_OFFSET_SAMPLE_RATE = 5
CONFIG_MASK_SAMPLE_RATE = 0x7 << CONFIG_OFFSET_SAMPLE_RATE

SAMPLE_RATE_BITS = {
	"ADS1015": {
		128:   0x00 << CONFIG_OFFSET_SAMPLE_RATE,
		250:   0x01 << CONFIG_OFFSET_SAMPLE_RATE,
		490:   0x02 << CONFIG_OFFSET_SAMPLE_RATE,
		920:   0x03 << CONFIG_OFFSET_SAMPLE_RATE,
		1600:  0x04 << CONFIG_OFFSET_SAMPLE_RATE,
		2400:  0x05 << CONFIG_OFFSET_SAMPLE_RATE,
		3300:  0x06 << CONFIG_OFFSET_SAMPLE_RATE
	},
	"ADS1115": {
		8:    0x00 << CONFIG_OFFSET_SAMPLE_RATE,
		16:   0x01 << CONFIG_OFFSET_SAMPLE_RATE,
		32:   0x02 << CONFIG_OFFSET_SAMPLE_RATE,
		64:   0x03 << CONFIG_OFFSET_SAMPLE_RATE,
		128:  0x04 << CONFIG_OFFSET_SAMPLE_RATE,
		250:  0x05 << CONFIG_OFFSET_SAMPLE_RATE,
		475:  0x06 << CONFIG_OFFSET_SAMPLE_RATE,
		860:  0x07 << CONFIG_OFFSET_SAMPLE_RATE
	}
}
SAMPLE_RATES = {chip: {
	b: r for (r, b) in SAMPLE_RATE_BITS[chip].items()
} for chip in SAMPLE_RATE_BITS}
DEFAULT_SAMPLE_RATES = {
	"ADS1015": 1600,
	"ADS1115": 128
}




################################################################
# TODO: Comparator advanced settings
################################################################


################################################################
# Comparator modes. 3 bits at offset 0
# Setting all bits disables the comparator
################################################################
CONFIG_OFFSET_COMPARATOR_Q = 0
CONFIG_MASK_COMPARATOR_Q = 0x07 << CONFIG_OFFSET_COMPARATOR_Q
COMPARATOR_Q_LENGTH_BITS = {
	1: 		0x00 << CONFIG_OFFSET_COMPARATOR_Q,
	2: 		0x01 << CONFIG_OFFSET_COMPARATOR_Q,
	4: 		0x02 << CONFIG_OFFSET_COMPARATOR_Q,
	None:	0x03 << CONFIG_OFFSET_COMPARATOR_Q
}
# the following is useful for reverse lookups
COMPARATOR_Q_LENGTHS = {b: l for (l, b) in COMPARATOR_Q_LENGTH_BITS.items()}




################################
# Register numbers
################################


REG_CONVERSION =		0x00	# can be 12 or 16 bit, for ADS1015 and ADS1115, respectively
REG_CONFIG =			0x01	# 16 bit


# comparator thresholds
REG_LOW_THRESHOLD =		0x02	# 16 bit
REG_HIGH_THRESHOLD =	0x03	# 16 bit









class ADS1x15():
	"""
		ADS1015/ADS1115 basic implementation.
	
	"""

	def __init__(self,
		smbus_connection,
		address = DEFAULT_I2C_ADDRESS,
		sample_rate = None,
		saturation_voltage = DEFAULT_SATURATION_VOLTAGE
	):
		"""
			
			Args:
				smbus_object:		(SMBus)The SMBus object used for i2c operations,
				address:		(int)The I2C address the device is configured on
				sample_rate:		(int)Data rate of the device
				voltage_range:		(float)The voltage range (+/-) (amplifier gain)
		"""


		self._bus = smbus_connection


		try:
			self._address = SUPPORTED_ADDRESSES[str(address)]
		except (ValueError, KeyError) as e_noaddr:
			raise ValueError("Unsupported address for %s: `%s`. Supported values are %s" %
				(self.__class__.__name__, address, ", ".join(
					[ ("`%s` (value: 0x%x)" % (a, SUPPORTED_ADDRESSES[a])) for a in sorted(SUPPORTED_ADDRESSES)]
				))
			)

		# this doubles as a check for unsupported subclassess
		try:
			self._sample_rate_bits = SAMPLE_RATE_BITS[self.__class__.__name__]
			self._sample_rates = SAMPLE_RATES[self.__class__.__name__]
			self._default_sample_rate = DEFAULT_SAMPLE_RATES[self.__class__.__name__]
		except KeyError as e_nosr:
			raise KeyError("No SAMPLE_RATE_BITS/DEFAULT_SAMPLE_RATES entry is defined for class %s" % self.__class__.__name__)


		# the following hold the bits at the correct offset
		self._gain = None
		self._srr = None


		# set values and let setters to their job
		self.saturation_voltage = saturation_voltage
		self._lsb_size = self.saturation_voltage / float(0x7fff)
		self.sample_rate = self._default_sample_rate if (sample_rate is None) else sample_rate
	

	def _saturation_voltage_get(self):
		return SATURATION_VOLTAGES[self._gain]


	def _saturation_voltage_set(self, voltage):

		try:
			self._gain = GAIN_LEVELS[float(voltage)]
		except (KeyError, ValueError) as e_novoltage:
			raise ValueError("Unsupported saturation voltage for %s: `%s`. Supported values are %s" %
				(self.__class__.__name__, voltage, ", ".join([ ("%.3f" % v) for v in sorted(GAIN_LEVELS)]))
			)

	saturation_voltage = property(
		fget = _saturation_voltage_get,
		fset = _saturation_voltage_set,
		doc = "Saturation voltage (sets the gain)"
	)

	def _sample_rate_get(self):
		return self._sample_rates[self._srr]

	def _sample_rate_set(self, rate):
		
		try:
			self._srr = self._sample_rate_bits[int(rate)]
		except (KeyError, ValueError) as e_norate:
			raise ValueError("Unsupported sample rate for %s: `%s`. Supported values are %s" %
				(self.__class__.__name__, rate, ", ".join([ ("%d" % r) for r in sorted(self._sample_rate_bits)]))
			)

	sample_rate = property(
		fget = _sample_rate_get,
		fset = _sample_rate_set,
		doc = "Data rate in Samples/S"
	)


		

	def get_voltage(self, channel, channel_d = None):
		"""
			Retrieves the voltage of a specific channel from ground
			or from a different channel in differential mode

			Args:
				channel:	(int)The channel whose positive voltage is requested
				channel_d:	(int)The negative voltage channel in differential mode.
							Only some pairs are supported for differential, see
							MUX_MODE_DIFFERENTIAL_BITS. None disables differential mode
		"""

		if (not (isinstance(channel, int) and (0 <= channel < TOTAL_CHANNELS))):
			raise ValueError("Invalid channel number: `%s` (must be a positive integer lower than %d)" % (
				channel, TOTAL_CHANNELS
			))

		# These are there no matter what
		# We only support one-shot for now (also 'cause continuous makes the other channels useless)
		config = (
			OPERATIONAL_STATUS_BITS[True] | self._gain |
			CONTINUOUS_MODE_BITS[False] | self._srr | COMPARATOR_Q_LENGTH_BITS[None]
		)


		if (channel_d is None):

			# channel-to-ground
			config |= (MUX_MODE_CHANNEL_NUMBER | (channel << CONFIG_OFFSET_MUX_MODE))

		else:

			if (not (isinstance(channel_d, int) and (0 <= channel_d < TOTAL_CHANNELS))):
				raise ValueError("Invalid differential channel number: `%s` (must be a positive number lower than %d)" % (
					channel_d, TOTAL_CHANNELS
				))

			try:
				config |= (MUX_MODE_DIFFERENTIAL_BITS[(channel, channel_d)])
			except KeyError as e_nopair:
				raise ValueError("Invalid channel pair for differential measurement: `%d - %d`. Only the following pairs are supported: %s" % (
					channel, channel_d, ", ".join([ ("%d - %d" % dp) for dp in sorted(MUX_MODE_DIFFERENTIAL_BITS)])
				))

		config_bytes = [(config >> 8) & 0xff, config & 0xff]
		
		self._bus.write_i2c_block_data(self._address, REG_CONFIG, config_bytes)



		# we wait until conversion is complete.
		converting = True
		while (converting):
			time.sleep(CONVERSION_POLL_INTERVAL)
			status_bytes = self._bus.read_i2c_block_data(
				self._address,
				REG_CONFIG,
				CONFIG_SIZE
			)
			status = ((status_bytes[0] & 0xff) << 8) | (status_bytes[0] & 0xff)
			converting = (CONVERTING_STATUSES[CONFIG_MASK_CONVERTING_STATUS & status])
			if (converting):
				continue


			channel_bytes = self._bus.read_i2c_block_data(
				self._address,
				REG_CONVERSION,
				SAMPLE_SIZE
			)

			break


		# First conversion step:
		# normalize all values into an identity vector with a +/- 1.0 range

		# 12 bit samples sacrifice precision and therefore lower-significance bits
		# We map everything against a 16 bit value. The lower 4 bits of 12bit
		# values mapped this way will always be 0
		channel_val = ((channel_bytes[0] & 0xff) << 8) | (channel_bytes[1] & 0xff)

		# if the sign bit is set, we shift
		if (channel_val & 0x8000):
			channel_val -= (1 << 16)

		# Time to convert the channel value to voltage
		return (self._lsb_size * float(channel_val))






class ADS1015(ADS1x15):
	""" Chip variation-specific subclass """


class ADS1115(ADS1x15):
	""" Chip variation-specific subclass """



# some monkey patches for classes that have data rates, allowing for reverse lookups
for chip_class in globals().copy().values():
	if (not isinstance(chip_class, type)):
		continue
	if (hasattr(chip_class, "RATE_REGISTERS")):
		setattr(
			chip_class,
			"REGISTER_RATES",
			{b: r for (r, b) in getattr(chip_class, "RATE_REGISTERS").items()}
		)
