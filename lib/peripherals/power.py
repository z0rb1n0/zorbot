#!/usr/bin/python3 -uB
"""
	Drivers to handle power modules
"""

import sys, os, time, logging


LOGGER = logging.getLogger(__name__)


# What follows are the properties that can be measured by a VMeter. They
# They are used to monkey-patch all the relevant methods.
# The key turns into a method name (there must be a matching <key>_meter
# argument in __init__'s signature)
#
#
# Each member tuple defines:
# - The label
# - The unit
# - Boolean that indicates if the divider must be applied
#   If None the divider is 1.0
# - Metric docstring

VSOURCE_METRICS = {
	"vdc": 			("VDC", "V", True, "Measured DC Voltage, or DC bias in a signal"),
	"vac": 			("VAC", "V", True, "Measured AC Voltage Amplitude, peak-to-peak"),
	"frequency": 	("Frequency", "Hz", False, "Measured Signal Frequency"),
	"current": 		("Current", "A", False, "Measured current, maximum value in case of alternate signal components")
}

class VSource():
	"""
		Base implementation & abstraction of voltage sources/lines/rails/VDC/VAC.
		Information about Voltage/Current and such is obtained through callables.
		
		All values are expressed in natural units (Volt, Hz, Ampere) as floats
	"""
	def __init__(self,
		vdc_meter = None,
		vac_meter = None,
		frequency_meter = None,
		current_meter = None,
		max_current = None,
		divider_ratio = 1.0,
		max_sample_age = 0.01
	):
		"""
			Simple initialisation: the callables to variuous meters are passed.
			
			Args:
			
				vdc_meter:			(callable)Anything returning a float indicating the DC voltage/bias
				vac_meter:			(callable)Anything returning a float indicating AC amplitude
				frequency_meter:	(callable)Anything returning a float indicating the frequency in hertz.
									The frequency meter is expected to return an already calculated
									value, which may cause long blocking operations if it's not offloaded to
									some component
				current_meter:		(callable)Anything returning a float indicating the current current draw
				max_current:		(float)Absolute maximum peak rated current, in Amps
				divider_ratio:		(float)If your voltage measurement circuit uses a voltage divider, the
									ratio between the divider's tapoff voltage and VDC can be specified here
				max_sample_age:		(float)Maximum age of sample values before an update is triggered
		"""

		passed_args = locals().copy()

		self.max_current = max_current
		self.divider_ratio = divider_ratio
		self.max_sample_age = max_sample_age

		self._ts_sample = None 					# timestamp of last sample
		self._meters = {} 						# Callables to get the values. Indexed by metric name
		self._sample = {} 						# Indexed by metric name


		for metric in VSOURCE_METRICS:
			metric_arg = metric + "_meter"
			# If someone forgets to define the argument when adding
			# VSOURCE_METRICS, then the entry is simply ignored
			if (metric_arg not in passed_args):
				continue
			arg_val = passed_args[metric_arg]
			if (not ((arg_val is None) or callable(arg_val))):
				raise TypeError("%s_meter meter must be a callable" % meter_arg)

			self._meters[metric] = arg_val




	

	fresh = property(
		fget = lambda self : ((self._ts_sample or 0.0) >= (time.time() - self.max_sample_age)),
		doc = "Indicates whether or not the sample is fresh"
	)


	def get_sample(self):
		"""
			Goes through the registered callables and updates the internal
			variables + the timestamp
			
			Returns:
				the new sample
		"""
		
		self._ts_sample = time.time() # we pessimistically do it before
		for (metric, meter) in self._meters.items():

			if (meter is None):
				v = None
			else:
				v = meter()
				# does this one care about the divider?
				if (VSOURCE_METRICS[metric][2]):
					v /= self.divider_ratio

			self._sample[metric] = v
		
		return self._sample

	def _sample_get(self):
		"""
			The sample getter causes an update if the sample is old
		"""
		if (not self.fresh):
			LOGGER.debug("Sample has expired. Retrieving new sample")

			return self.get_sample().copy()

		return self._sample.copy()

	sample = property(
		fget = _sample_get,
		doc = "The sample itself, as a dictionary indexed by metric name"
	)

	def get_sample_metric(self, metric):
		"""
			Generalized function to retrieve a single metric from the current sample.
			
			Args:
				metric:		(str)An index in VSOURCE_METRICS
	
			Returns:
				the metric value
		"""
		return self.sample[metric]



# monkey patching the methods in
def patch_metric_getter():
	for metric in VSOURCE_METRICS:
		getter_name = "_%s_get" % metric

		def scope_jail(metric_name):
			def next_getter(self):
				# each time `metric_name` resolves to a different instance
				return self.get_sample_metric(metric_name)
			return next_getter
		next_getter = scope_jail(metric_name = metric)
		next_getter.__name__ = getter_name
		setattr(VSource, next_getter.__name__, next_getter)

		# register the 
		setattr(VSource, metric, property(
			fget = getattr(VSource, getter_name),
			doc = VSOURCE_METRICS[metric][3] # docstring
		))
patch_metric_getter()
del(patch_metric_getter)

