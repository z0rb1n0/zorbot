#!/usr/bin/python3 -uB
"""
	Drivers to handle propulsion devices (wheels, props...)
"""

import sys, os, time, logging

LOGGER = logging.getLogger(__name__)

from . import \
	power


from bus import \
	gpio


DEFAULT_DC_THRUSTER_MIN_VOLTAGE = 2.0
DEFAULT_DC_THRUSTER_MAX_VOLTAGE = 3.0

class ElectricalException(Exception):
	""" Gross electrical anomalies """




class Thruster():
	""" Base thruster class implementation stub """

class DCThruster(Thruster):
	"""
		Temporary implementation.
		The proper one will probably require multiple inheritance involving
		motors and power sources. Also, gimballing is not implemented yet
	"""

	def __init__(self,
		power_rail,
		control_pin,
		min_voltage = DEFAULT_DC_THRUSTER_MIN_VOLTAGE,
		max_voltage = DEFAULT_DC_THRUSTER_MAX_VOLTAGE
	):
		"""
			Sets up a DC thruster
			
			Args:
				power_rail:		(VSource)The power source feeding this thruster
				control_pin:	(Pin)The PWM pin controllin the thruster
				min_voltage:	(float)The minimum voltage allowing motor operator. Includes voltage drops from amplifiers/etc
				man_voltage:	(float)The maximum voltage for safe motor operators. Includes voltage drops from amplifiers/etc
			
		"""

		if (not isinstance(power_rail, power.VSource)):
			raise TypeError("power_rail must be a VSource")

		if (not isinstance(control_pin, gpio.Pin)):
			raise TypeError("power_rail must be a Pin")

		self.power_rail = power_rail
		self.control_pin = control_pin

		# the following two are immutable
		self._min_voltage = min_voltage
		self._max_voltage = max_voltage

		self._thrust_fraction = None
		self._motor_duty_cycle = None
		
	min_voltage = property(fget = lambda self : self._min_voltage, doc = "Minimum DC to operate the motor. Includes voltage drops from amplifiers/etc")
	max_voltage = property(fget = lambda self : self._max_voltage, doc = "Maximum DC voltage the motor can tolerate. Includes voltage drops from amplifiers/etc")



	def _thrust_fraction_get(self):
		return self._thrust_fraction

	def _thrust_fraction_set(self, fraction):

		pr_vdc = self.power_rail.vdc
		if ((pr_vdc or (self._min_voltage - 1.0)) <= self._min_voltage):
			LOGGER.warning("Insufficient rail voltage for DC motor: %.3fV (%.3fV or more is required). Engine will shut down",
				pr_vdc, self._min_voltage
			)
			control_pin.value = 0
			return None

		dc_multiplier = min(self._max_voltage / pr_vdc, 1.0)

		self._thrust_fraction = fraction or 0.0
		self._motor_duty_cycle = dc_multiplier * self._thrust_fraction
		LOGGER.debug("Thruster set thrust: %4.2f%%; Rail voltage: %.3fV; Motor duty cycle multiplier: %.3f; Effective voltage: %.3f",
			100.0 * self._thrust_fraction, pr_vdc, dc_multiplier, pr_vdc * self._motor_duty_cycle
		)
		self.control_pin.value = self._motor_duty_cycle


	thrust_fraction = property(
		fget = _thrust_fraction_get,
		fset = _thrust_fraction_set,
		doc = "Current thrust fraction of the maximum thurst capacity"
	)

	def _motor_duty_cycle_get(self):
		return self._motor_duty_cycle
	
	motor_duty_cycle = property(fget = _motor_duty_cycle_get, doc = "Current PWM duty cycle of the motor. Cannot be set directly")


	def shutdown(self):
		if (self.thrust_fraction > 0.0):
			LOGGER.debug("Shutting down thruster")
			self._thrust_fraction_set(0)
		


	def __del__(self):
		self.shutdown()
