#!/usr/bin/python3 -uB
"""
	Driver classes to handle the bot's main support bus/structure
"""

import sys, os, enum, time, logging


LOGGER = logging.getLogger(__name__)


from peripherals import \
	power, propulsion



class Frame():
	""" Generic object to represent a robot system physical layout."""
	def __init__(self,
		devices = None
	):
		"""
			Just preloads things

			Args:
				devices:		(one of power.VSource, propulsion.Thruster)
								Dictionary of devices to attach to this bus at initialization.
								Their class will determine what group internal list it will
								end up in (errors if unsupported).
								Identification keys must be supplied (will be always cast to strings)
		"""
		

		self.devices = {} 			# global list of devices
		self.power_rails = {}		# power rails aliased here
		self.thrusters = {}			# thrusters aliased here

		if (not isinstance(devices or {}, dict)):
			raise TypeError("devices must be a dictionary of devices, not a %s" % devices.__class__.__name__)

		for (k, d) in devices.items():
			self.attach(id = k, device = d)

	

	def attach(self, id, device):
		"""
			Attach one device to the relevant member.
			
			Args:
				id:			(string)Identification string for the device in the frame
				device:		(many)Device. Only some classes are supported
		"""

		if (id is None):
			raise ValueError("id cannot be None")

		# here we route the alias
		if (isinstance(device, power.VSource)):
			alias_member = self.power_rails
		elif (isinstance(device, propulsion.Thruster)):
			alias_member = self.thrusters
		else:
			raise TypeError("Unsupported device class: %s" % device.__class__.__name__)


		id_str = str(id)
		if (id_str in self.devices):
			raise KeyError("Device with ID `%s` already exist on this %s" % (
				id_str, self.__class__.__name__
			))

		self.devices[id_str] = device
		alias_member[id_str] = self.devices[id_str]
	

	def detach(self, id, force_error = False):
		"""
			Opposite of attach
			
			Args:
				id:				(str)Device ID to detach
				force_error:	(bool)Throw an error if the device does not exist
		"""
		id_str = str(id)
		if (id_str not in self.devices):
			if (force_error):
				raise KeyError("No such device ID: `%s`" % id_str)
			return

		# first remove all aliases
		for alias_m in [self.power_rails, self.thrusters]:
			if (id_str in alias_m):
				del(alias_m[id_str])

		# then the device
		del(self.devices[id_str])

	def shutdown(self):
		
		"""
			Shuts down all devices and powers off
		"""
		LOGGER.info("Shutting down propulsion and deleting devices")
		for (id_str, device) in self.devices.copy().items():
			if (hasattr(device, "shutdown")):
				device.shutdown()
			del(self.devices[id_str])
