#!/usr/bin/python3 -uB
"""
	Drivers to handle propulsion devices (wheels, props...)
"""

import sys, os, time, logging, pigpio, enum


LOGGER = logging.getLogger(__name__)


class StateError(Exception):
	""" System in the wrong state """

@enum.unique
class IOMode(enum.IntEnum):

	InputDigital = 1
	OutputDigital = 3
	PWM = 4
	ServoControl = 5


@enum.unique
class WiringMode(enum.IntEnum):

	Floating = 0
	PullDown = 1
	PullUp = 2



DEFAULT_IO_MODE = IOMode.InputDigital
DEFAULT_WIRING_MODE = WiringMode.Floating
DEFAULT_PWM_FREQUENCY = 10000


# maximum value of PWM duty cycle
PWM_DC_RANGE = (1<<12) - 1 


SERVO_OFF = 0
SERVO_LIMIT_CCW = 500
SERVO_LIMIT_CW = 2500


GPIO_SCAN_MAX = 64

# list of pins that are pure GPIO and can be safely initialized
SAFE_PINS = {
	5, 6, 12, 13, 16, 17, 18,
	22, 23, 24, 25, 26, 27
}



class Pin():
	def __init__(self,
		parent_bus,
		number,
		io_mode = DEFAULT_IO_MODE,
		wiring_mode = DEFAULT_WIRING_MODE,
		pwm_frequency = DEFAULT_PWM_FREQUENCY
	):
		"""
			Configures the pin if its a safe one

			Does not touch any configuration if it's a system pin

			Args:
				parent_bus:					(GPIO)Object this pin is mapped to
				number:						(int)Pin number, chip numbering
				io_mode:					(IOMode)Mode to initialize the pin to. Ignored for
											multi-purpose pin as the mode is always discovered for those
				wiring_mode:				(WiringMode)What to wire the pin to by default.
				pwm_frequency:				(int)When PWM is used, this is the frequency
											is set to this value
				
		"""
		
		self._parent_bus = parent_bus
		self._number = number

		# This can be set to OutputDigital, PWM or ServoControl as the API does not track them.
		# The value is None for Input pins
		self._write_mode = None
		

		# the following are used only when the motor is switched to PWM/ServoControl
		self._start_pwm_frequency = pwm_frequency

		if (self.number in SAFE_PINS):
			self.io_mode = io_mode
			self.wiring_mode = wiring_mode
		else:
			LOGGER.info("Pin #%d is not GPIO-exclusive and therefore unsafe. Ignoring supplied initial configuration")


	def _parent_bus_get(self):
		return self._parent_bus

	parent_bus = property(
		fget = _parent_bus_get,
		doc = "Bus object this pin is attached to"
	)

	def _number_get(self):
		return self._number
	number = property(
		fget = _number_get,
		doc = "SoC Pin number"
	)


	def _io_mode_get(self):

		try:
			hw_mode = self.parent_bus._pigpio.get_mode(self.number)
		except pigpio.error as e_noget:
			raise IOError("Unable to get I/O mode for pin #%d: %s(%s)" % (
				self.number, e_noget.__class__.__name__, e_noget
			))

		out_mode = {
			pigpio.INPUT: IOMode.InputDigital,
			pigpio.OUTPUT: IOMode.OutputDigital
		}.get(hw_mode)
		

		if (out_mode is None):
			raise IOError("Invalid I/O mode was retrieved from pin #%d: %s" % (
				self._number, hw_mode
			))
			
		if (out_mode == IOMode.OutputDigital):
			# the truth is in a member...
			out_mode = self._write_mode
			

		return out_mode


	def _io_mode_set(self, mode):

		tgt_mode = {
			IOMode.InputDigital: pigpio.INPUT,
			IOMode.OutputDigital: pigpio.OUTPUT,
			IOMode.PWM: pigpio.OUTPUT,
			IOMode.ServoControl: pigpio.OUTPUT
		}.get(mode)

		if (tgt_mode is None):
			raise KeyError("Invalid Pin mode: %s" % mode)

		if (self._write_mode is not None):
			# This will reset any kind of value to safe output before switching mode,
			# as it works for all of Digital, PWM and ServoControl
			self._value_set(None)

		# we need to track this ourselves as the library does not
		self._write_mode = mode if (mode in [
			IOMode.OutputDigital,
			IOMode.PWM,
			IOMode.ServoControl
		]) else None

		try:
			self.parent_bus._pigpio.set_mode(self.number, tgt_mode)
			LOGGER.debug("I/O mode set to %s on pin #%d", mode.name, self._number)
		except pigpio.error as e_noset:
			raise IOError("Unable to set I/O mode for pin #%d: %s(%s)" % (
				self.number, e_noset.__class__.__name__, e_noset
			))

		if (self._write_mode == IOMode.PWM):

			# this is fixed and does not need a public method
			self.parent_bus._pigpio.set_PWM_range(self._number, PWM_DC_RANGE)

			self._pwm_frequency_set(self._start_pwm_frequency)

		return False
	
	io_mode = property(
		fget = _io_mode_get,
		fset = _io_mode_set,
		doc = "Pin I/O mode"
	)


	def _wiring_mode_set(self, mode):

		if (self.io_mode not in [IOMode.InputDigital]):
			final_mode = WiringMode.Floating
			# output pin. We leave it floating anyway
			LOGGER.info("Wiring mode forced to %s for pin #%d as an output I/O mode is in effect",
				final_mode.name, self._number
			)
		else:
			final_mode = mode

		tgt_mode = {
			WiringMode.Floating: pigpio.PUD_OFF,
			WiringMode.PullDown: pigpio.PUD_DOWN,
			WiringMode.PullUp: pigpio.PUD_UP
		}.get(final_mode)

		if (tgt_mode is None):
			raise KeyError("Invalid Wiring Mode: %s" % final_mode)

		try:
			self.parent_bus._pigpio.set_pull_up_down(self.number, tgt_mode)
			LOGGER.debug("Wiring mode set to %s on pin #%d", final_mode.name, self._number)
		except pigpio.error as e_noset:
			raise IOError("Unable to set wiring mode for pin #%d: %s(%s)" % (
				self.number, e_noset.__class__.__name__, e_noset
			))


	wiring_mode = property(
		fset = _wiring_mode_set,
		doc = "Wiring Mode to pull up/down resistors. Write-only"
	)


	def _pwm_frequency_get(self):

		if (self._write_mode not in [IOMode.PWM]):
			raise StateError("Pin #%d is not in PWM mode" % self._number)

		try:
			hz = self.parent_bus._pigpio.get_PWM_frequency(self.number)
		except pigpio.error as e_noget:
			raise IOError("Unable to get PWM frequency for pin #%d: %s(%s)" % (
				self.number, e_noget.__class__.__name__, e_noget
			))

		return hz


	def _pwm_frequency_set(self, hz):
		
		if (not (isinstance(hz, int) and (hz > 0))):
			raise ValueError("Invalid PWM frequency: `%s`" % hz)

		if (self._write_mode not in [IOMode.PWM]):
			raise StateError("Pin #%d is not in PWM mode" % self._number)

		try:
			new_hz = self.parent_bus._pigpio.set_PWM_frequency(self._number, hz)

			if (new_hz in [pigpio.PI_BAD_USER_GPIO, pigpio.PI_NOT_PERMITTED]):
				raise IOError("pigpio.set_PWM_frequency() returned %d" % new_hz)

			if ((float(max(new_hz, hz)) / float(min(new_hz, hz))) > 1.30):
				LOGGER.warning("Frequency approximation to nearest supported one exceeding 30%% on pin #%d: requested %d, set %d",
					self._number, hz, new_hz
				)
			else:
				LOGGER.debug("PWM frequency set to %dHz on pin #%d", new_hz, self._number)
		except pigpio.error as e_noset:
			raise IOError("Unable to set frequency for pin #%d: %s(%s)" % (
				self.number, e_noset.__class__.__name__, e_noset
			))

	pwm_frequency = property(
		fget = _pwm_frequency_get,
		fset = _pwm_frequency_set,
		doc = "PWM frequency, in Hz. Note that the supplied frequency will be approximated to the nearest discrete value supported by pigpio"
	)

	
	def _value_set(self, value):
		if (self._write_mode is None):
			raise StateError("Pin #%d is in an input I/O mode and cannot be set" % self._number)

		if (self._write_mode == IOMode.OutputDigital):

			# plain bit
			self.parent_bus._pigpio.write(self._number, pigpio.HIGH if (value) else pigpio.LOW)

		elif (self._write_mode == IOMode.PWM):

			if (not (
				(isinstance(value, float) and (0.0 <= value <= 1.0))
			or
				(value in {None, 0, 1})
			)):
				raise ValueError("Pin #%d is in %s mode. Valid values are floats in the 0.0 - 1.0 range or None" % (
					self._number, self._write_mode.name
				))
			
			# translation to range
			self.parent_bus._pigpio.set_PWM_dutycycle(
				self._number,
				int(round(float(PWM_DC_RANGE) * (0.0 if (value is None) else float(value))))
			)
				
				
		elif (self._write_mode == IOMode.ServoControl):

			if (not (
				(isinstance(value, float) and (-1.0 <= value <= 1.0))
			or
				(value in {None, 0, -1, 1})
			)):
				raise ValueError("Pin #%d is in %s mode. Valid values are floats in the -1.0 - 1.0 range or None" % (
					self._number, self._write_mode.name
				))

			# servo standard values
			self.parent_bus._pigpio.set_servo_pulsewidth(
				self._number,
				(0 if (value is None) else (1500 + int(round(1000.0 * float(value)))))
			)

	def _value_get(self):

		if (self._write_mode is not None):
			raise StateError("Pin #%d is in an output mode and cannot be read" % self._number)



		return bool(self.parent_bus._pigpio.read(self._number))
			


	value = property(
		fget = _value_get,
		fset = _value_set,
		doc = """

			This is the most magical of all membersas both its type and its
			semantics entirely depend on the Pin modes.
			
			Rulse when set:

				For digital output pins, plain boolean evaluation of the value is used.
				None is equivalent to LOW

				For PWM pins, a normalized float 0.0-1.0 is expected as the value
				for the duty cycle. None is equivalent to 0.0
				
				For ServoControl pins, a range of floats between -1.0 and 1.0 is expected,
				mapping to the typical 500-2500 values. None is equivalent to 0
				(disables the servo).
				WARNING!!!: the value 0, even when passed as an integer, represents the
				servo middle point and not STOP. Only None does that

			Rules when read:
			
				At the moment it is only possible to read from pins in InputDigital
				states (they return a boolean).
				Although it would be possible to retrieve values such as the
				duty cycle and servo control value, it has a significant performance
				penalty for a setting that presumably the caller already knows

		"""
	)


	def stop(self):
		# cleanup
		if (self._write_mode is not None):
			LOGGER.debug("Setting pin #%d to a quiescent state", self._number)
			self._value_set(None)
			self._io_mode_set(IOMode.InputDigital)
			self._wiring_mode_set(WiringMode.PullDown)
		
	def __del__(self):
		try:
			self.stop()
		except Exception as e_latederef:
			# eh...
			pass


class GPIO():
	def __init__(self,
		default_io_mode = DEFAULT_IO_MODE,
		default_wiring_mode = DEFAULT_WIRING_MODE,
		default_pwm_frequency = DEFAULT_PWM_FREQUENCY,
		safe_mode = True
	):
		"""
			Initialize pigio and load pins.
			
			Safe mode only loads pure GPIO pins that don't serve other purposes
			
			Args:
				default_io_mode:			(IOMode)What mode to set pins to upon initialization.
											This option is ignored by unsafe pins
				default_wiring_mode:		(WiringMode)What to wire the pins to (if at all) at initialization
											This option is ignored by unsafe pins
				default_pwm_frequency:		(int)Any discovered Pin (except unsafe ones)
											is initially configured at this PWM frequency.
											This option is ignored by unsafe pins
				safe_mode:					(bool)Skip unsafe (multi-purpose) pins during
											discovery
		"""

		self.default_pwm_frequency = int(default_pwm_frequency)
		self.safe_mode = safe_mode

		self._pigpio = pigpio.pi()

		self.pins = dict()

		self.rescan(safe_mode = self.safe_mode)
		


	def rescan(self, safe_mode = True):
		for pin_id in range(GPIO_SCAN_MAX):
			# trial and error
			try:
				self._pigpio.read(pin_id)
				if ((not safe_mode) or (pin_id in SAFE_PINS)):
					self.pins[pin_id] = Pin(
						parent_bus = self, # give the GC a field day...
						number = pin_id,
						pwm_frequency = self.default_pwm_frequency
					)
					LOGGER.debug("Pin #%d regitered" % pin_id)
				else:
					LOGGER.debug("Pin #%d is unsafe and safe mode is enabled. Skipping" % pin_id)
			except pigpio.error as e_nopin:
				LOGGER.debug("Pin #%d is not available" % pin_id)
				if (pin_id in self.pins):
					LOGGER.warning("Pin #%d disappeared. Removing" % pin_id)
					del(self.pins[pin_id])



	def close(self):
		for pin_id in self.pins.copy():
			self.pins[pin_id].stop()
			del(self.pins[pin_id])
		self._pigpio.stop()

	def __del__(self):
		try:
			self.close()
		except Exception as e_latederef:
			# eh...
			pass
		
		