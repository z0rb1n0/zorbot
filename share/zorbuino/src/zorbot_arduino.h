#define SERIAL_MODE SERIAL_8N1


/*
 * Command types
 *
 *
 */

#define PROTOCOL_VERSION 1


typedef enum op_code{
	CMD_GET_PROTOCOL_VERSION		= 1,		// get server version
	CMD_SET_PORT_MODE				= 5,		// request change in baud rate (only applicable to serial/UART) and bit mode
	CMD_SET_FLAGS					= 7,		// change session flags. As of 2018-07-17 the only supported flag is 0: binary mode
	CMD_SET_PIN_MODES				= 10,		// set Pin modes
	CMD_READ_VALUES					= 15,		// get information about INPUTs
	CMD_SET_VALUES					= 20		// set Pin value
} op_code;



#define FLAG_BINARY_MODE 1



/*
 * Response states.
 * Anything except RESP_OK sends back a message text
 *
 */
typedef enum response_code{
	RESP_OK							= 0,		// everything OK, rest of the message is data if that's needed
	RESP_PARSE_ERROR				= 1,		// command failed to parse
	RESP_INTERNAL_ERROR				= 4,		// something went wrong
	RESP_INVALID_STATE				= 7			// wrong state for the operation (generally PINSin the wrong I/Ostate)
} response_code;


typedef enum pin_mode {
	PIN_MODE_INPUT = INPUT,
	PIN_MODE_INPUT_PULLUP = INPUT_PULLUP,
	PIN_MODE_OUTPUT = OUTPUT
} pin_mode;



typedef enum uart_bps{
	UART_BPS_300 = 300,
	UART_BPS_600 =  600,
	UART_BPS_1200 = 1200,
	UART_BPS_2400 = 2400,
	UART_BPS_4800 = 4800,
	UART_BPS_9600 = 9600,
	UART_BPS_14400 = 14400,
	UART_BPS_19200 = 19200,
	UART_BPS_28800 = 28800,
	UART_BPS_38400 = 38400,
	UART_BPS_57600 = 57600,
	UART_BPS_115200 = 115200
} baud_bps;

// taken directly from the arduino manual
typedef enum uart_mode{
	UART_MODE_5N1 = SERIAL_5N1,
	UART_MODE_6N1 = SERIAL_6N1,
	UART_MODE_7N1 = SERIAL_7N1,
	UART_MODE_8N1 = SERIAL_8N1,
	UART_MODE_5N2 = SERIAL_5N2,
	UART_MODE_6N2 = SERIAL_6N2,
	UART_MODE_7N2 = SERIAL_7N2,
	UART_MODE_8N2 = SERIAL_8N2,
	UART_MODE_5E1 = SERIAL_5E1,
	UART_MODE_6E1 = SERIAL_6E1,
	UART_MODE_7E1 = SERIAL_7E1,
	UART_MODE_8E1 = SERIAL_8E1,
	UART_MODE_5E2 = SERIAL_5E2,
	UART_MODE_6E2 = SERIAL_6E2,
	UART_MODE_7E2 = SERIAL_7E2,
	UART_MODE_8E2 = SERIAL_8E2,
	UART_MODE_5O1 = SERIAL_5O1,
	UART_MODE_6O1 = SERIAL_6O1,
	UART_MODE_7O1 = SERIAL_7O1,
	UART_MODE_8O1 = SERIAL_8O1,
	UART_MODE_5O2 = SERIAL_5O2,
	UART_MODE_6O2 = SERIAL_6O2,
	UART_MODE_7O2 = SERIAL_7O2,
	UART_MODE_8O2 = SERIAL_8O2
} uart_mode;



typedef struct __attribute__((packed)) message {
	unsigned char length;
	unsigned char code;		// could be an op code or a response code
	char *body;
	unsigned char checksum;
} message_frame;
