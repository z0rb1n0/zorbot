#include "zorbot_arduino.h"
extern "C" {
#include "util/crc8.h"
}


#define tprintf(fmt, ...) _tprintf(PSTR(fmt), ##__VA_ARGS__)


char session_flags = 0;
char overhead_bytes = 6; // steps down to three in binary mode


// convenience "printf-like"
int serialputc(char c, FILE *fp);
void _tprintf(const char *fmt, ...);


// convenience "printf-like"

int serialputc(char c, FILE *fp) {
static char pc = 0; // last char printed

	
	(c == '\n') && (pc != '\r') && Serial.write('\r');
	Serial.write(c);
	pc = c;
	return 0;
}

void _tprintf(const char *fmt, ...) {
FILE stdiostr;
va_list ap;

  fdev_setup_stream(&stdiostr, serialputc, NULL, _FDEV_SETUP_WRITE);

  va_start(ap, fmt);
  vfprintf_P(&stdiostr, fmt, ap);
  va_end(ap);
}


// calculates the checksum and sends the message off
void send_response(message *res) {
byte *res_str; // Apparently Serial.write only accepts this
unsigned char o;

	
	res->checksum = 0;

	crc8(&res->checksum, res->length);
	crc8(&res->checksum, res->code);

	if (session_flags & FLAG_BINARY_MODE) {
		res_str = (byte *) res;
		for (o = 0; o < res->length - 3; o++) {
			crc8(&res->checksum, 2 + ((unsigned char) res_str[o]));
		}
		Serial.write(res_str, res->length);
	} else {
		// much less efficient
		tprintf("%02x%02x%s%02x\n",
			// + 3 bytes due to hex duplication
			res->length + 3, res->code, res->body, res->checksum
		);
	}

}

void setup() {
	// try to initialise the serial bus. Forever
	if (! (Serial)) {
		delay(1000);
	}
	Serial.begin(UART_BPS_57600, UART_MODE_8N1);
	Serial.setTimeout(1000);
	tprintf("Initializing\n");
}



void loop() {
unsigned char next_length;
char *next_buf;
unsigned char b_to_go;
	
// the two buffers
message request;
message response;


	// this stays allocated so long as we need to try and read hex input
	// read two bytes and scanf
	
	next_buf = 0;
	if (session_flags & FLAG_BINARY_MODE) {

		// single char read. Easy peasy
		while (! Serial.readBytes((char *) &next_length, 1));

	} else {
		// this is tricky: we keep feeding the next character in the buffer

		next_buf = (char *)malloc(2);

		b_to_go = 2;
		while (b_to_go) {
			b_to_go = b_to_go - Serial.readBytes(&next_buf[2 - b_to_go], b_to_go);
		}

		if (! sscanf(next_buf, "%02hhx", &next_length)) {
			// this will error later due to being too short
			next_length = 0;
		}
		
	}
	// we'll need this buffer anyway
	if (next_buf) {
		response.body = (char *)realloc(next_buf, 0xff - (sizeof(char) * 3));
	} else {
		response.body = (char *)malloc(0xff - (sizeof(char) * 3));
	}

	if (next_length < 3) {

		response.code = RESP_PARSE_ERROR;
		response.length = 3 + sprintf(
			response.body, (char *) "Invalid message length: %2x",

			next_length
		);

	} else {
		
		request.length = next_length;

		// again, keep reading we you got our data
		b_to_go = request.length - (session_flags & FLAG_BINARY_MODE) ? 3 : 6;
		while (b_to_go) {
			b_to_go = b_to_go - Serial.readBytes(&request.body, b_to_go - 3);
		}
		
		// and now read the 
		
		response.code = RESP_OK;
		

	}
	send_response(&response);
	free(response.body);
	
	
}
